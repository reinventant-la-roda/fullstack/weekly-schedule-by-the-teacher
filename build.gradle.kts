import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

plugins {
    val kotlinVersion = libs.versions.kotlin.get()
    kotlin("multiplatform") version kotlinVersion apply false
    kotlin("plugin.serialization") version kotlinVersion apply false
    id("com.github.ben-manes.versions") version libs.versions.versions.get()
    id("org.jetbrains.kotlinx.kover") version libs.versions.kover.get() apply false
}

group = "org.weekly.schedule"

subprojects {
    repositories {
        mavenCentral()
    }
}

// https://github.com/ben-manes/gradle-versions-plugin
tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf {
        isNonStable(candidate.version)
    }
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { it in version.uppercase() }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}
