package org.weekly.schedule.weekTime.domain

import org.weekly.schedule.dayTime.domain.DayTime

data class WeekTime(val week: WeekDay, val time: DayTime): Comparable<WeekTime> {

    override operator fun compareTo(other: WeekTime): Int {
        week.compareTo(other.week).let { if (it != 0) return it }
        return time.compareTo(other.time)
    }

    override fun toString(): String = "$week $time"

    companion object {
        fun of(week: String, time: String): WeekTime = WeekTime(
            week = WeekDay.of(week),
            time = DayTime.of(time)
        )
    }
}
