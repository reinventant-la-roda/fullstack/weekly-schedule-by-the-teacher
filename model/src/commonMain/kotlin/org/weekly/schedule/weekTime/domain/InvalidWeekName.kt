package org.weekly.schedule.weekTime.domain

class InvalidWeekName(name: String, allWeeks: List<String>) :
    Exception("Ha introduït la setmana $name. Però només acceptem els següents: $allWeeks")
