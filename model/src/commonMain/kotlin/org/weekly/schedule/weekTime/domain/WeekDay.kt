package org.weekly.schedule.weekTime.domain

enum class WeekDay(val day: String) {
    MONDAY("dilluns"),
    TUESDAY("dimarts"),
    WEDNESDAY("dimecres"),
    THURSDAY("dijous"),
    FRIDAY("divendres"),
    SATURDAY("dissabte"),
    SUNDAY("diumenge");

    override fun toString(): String = day

    companion object {
        fun of(week: String): WeekDay {
            return entries
                .firstOrNull { it.day == week.lowercase() }
                ?: throw InvalidWeekName(week, entries.map(WeekDay::day))
        }

        fun of(week: Int): WeekDay = try {
            entries[week]
        } catch (_: IndexOutOfBoundsException) {
            throw InvalidWeekNumber(week)
        }
    }
}
