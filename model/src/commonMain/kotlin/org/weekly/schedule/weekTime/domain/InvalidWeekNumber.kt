package org.weekly.schedule.weekTime.domain

class InvalidWeekNumber(week: Int) :
    Exception(
        "Per inicialitzar la setmana amb un dígit ha de ser compres entre el 0 i el 6 ambdós compresos.\n" +
                "Vostè ha introduït: $week"
    )
