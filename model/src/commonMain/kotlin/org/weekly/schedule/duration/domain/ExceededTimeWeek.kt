package org.weekly.schedule.duration.domain

class ExceededTimeWeek(duration: UInt) :
    Exception("Temps excedit d'una setmana, una setmana té un total de 604800s i vostè ha introduït: $duration")
