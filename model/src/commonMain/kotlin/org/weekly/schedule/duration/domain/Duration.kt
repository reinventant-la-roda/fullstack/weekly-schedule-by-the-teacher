package org.weekly.schedule.duration.domain

data class Duration(val weekSeconds: UInt) {
    init {
        if (weekSeconds == 0u) throw NeedPositiveDuration()
        if (weekSeconds >= 7u * 24u * 60u * 60u) throw ExceededTimeWeek(weekSeconds)
    }

    override fun toString(): String {
        val seconds = (weekSeconds % 60u).let { if (it != 0u) " ${it}s" else "" }
        val minutes = (weekSeconds / 60u % 60u).let { if (it != 0u) " ${it}m" else "" }
        val hours = (weekSeconds / (60u * 60u) % 24u).let { if (it != 0u) " ${it}h" else "" }
        val days = (weekSeconds / (60u * 60u * 24u)).let { if (it != 0u) "${it}d" else "" }

        return "$days$hours$minutes$seconds".trim()
    }

    companion object {
        fun of(init: String): Duration {
            if (init.isBlank()) throw EmptyDurationConfiguration()
            val map = init
                .split("""\s++""".toRegex())
                .associate { Pair(it.last(), it.dropLast(1).toUInt()) }
            val days = map['d'] ?: 0u
            val hours = map['h'] ?: 0u
            val minutes = map['m'] ?: 0u
            val seconds = map['s'] ?: 0u
            return Duration(
                seconds +
                        60u * (minutes +
                        60u * (hours +
                        24u * days
                        ))
            )
        }
    }
}
