package org.weekly.schedule.duration.domain

class NeedPositiveDuration : Exception("Definim que la durada només té sentit si és major que zero.")
