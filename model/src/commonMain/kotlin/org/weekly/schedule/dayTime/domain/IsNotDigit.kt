package org.weekly.schedule.dayTime.domain

class IsNotDigit(nonDigit: String, what: String) :
    Exception("Requereix d'un dígit per definir $what. Ha introduit $nonDigit")
