package org.weekly.schedule.dayTime.domain

class InvalidMinuteSeconds(seconds: Int) :
    Exception("Els segons d'un minut només poden ser representats amb dígits de 0 a 59. Ha introduït $seconds")
