package org.weekly.schedule.dayTime.domain

data class DayHours(val hours: UByte) {
    init {
        checkHours(hours.toInt())
    }

    override fun toString(): String = hours.toString()
    operator fun compareTo(other: DayHours): Int = hours.compareTo(other.hours)

    companion object {
        fun of(hours: Int): DayHours {
            checkHours(hours)
            return DayHours(hours.toUByte())
        }

        fun of(hours: String): DayHours = hours
            .toIntOrNull()
            ?.let(DayHours::of)
            ?: throw IsNotDigit(hours, "les hores")

        private fun checkHours(hours: Int) {
            if (hours !in 0..23) throw InvalidDayHour(hours)
        }
    }
}
