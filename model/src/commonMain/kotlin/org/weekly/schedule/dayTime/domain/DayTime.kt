package org.weekly.schedule.dayTime.domain

data class DayTime(val hours: DayHours, val minutes: HourMinutes, val seconds: MinuteSeconds) : Comparable<DayTime> {
    override fun toString(): String {
        if (seconds.isZero() && minutes.isZero()) return hours.toString()
        if (seconds.isZero()) return "$hours:$minutes"
        return "$hours:$minutes:$seconds"
    }

    override operator fun compareTo(other: DayTime): Int {
        hours.compareTo(other.hours).let { if (it != 0) return it }
        minutes.compareTo(other.minutes).let { if (it != 0) return it }
        return seconds.compareTo(other.seconds)
    }

    companion object {
        fun of(time: String): DayTime {
            val split = time.split(":")
            return when (split.size) {
                1 -> DayTime(DayHours.of(split[0]), HourMinutes.of(0), MinuteSeconds.of(0))
                2 -> DayTime(DayHours.of(split[0]), HourMinutes.of(split[1]), MinuteSeconds.of(0))
                3 -> DayTime(DayHours.of(split[0]), HourMinutes.of(split[1]), MinuteSeconds.of(split[2]))
                else -> throw InvalidDayTime(time)
            }
        }
    }
}
