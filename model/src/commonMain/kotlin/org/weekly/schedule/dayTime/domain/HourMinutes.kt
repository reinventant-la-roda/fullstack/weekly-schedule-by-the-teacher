package org.weekly.schedule.dayTime.domain

data class HourMinutes(val minutes: UByte) {
    init {
        checkMinutes(minutes.toInt())
    }

    fun isZero(): Boolean = minutes.toInt() == 0
    override fun toString(): String = minutes.toString()
    operator fun compareTo(other: HourMinutes): Int = minutes.compareTo(other.minutes)

    companion object {
        fun of(minutes: Int): HourMinutes {
            checkMinutes(minutes)
            return HourMinutes(minutes.toUByte())
        }

        fun of(minutes: String): HourMinutes = minutes
            .toIntOrNull()
            ?.let(HourMinutes::of)
            ?: throw IsNotDigit(minutes, "els minuts")

        private fun checkMinutes(minutes: Int) {
            if (minutes !in 0..59) throw InvalidHourMinutes(minutes)
        }
    }
}
