package org.weekly.schedule.dayTime.domain

class InvalidDayTime(time: String) :
    Exception("Hi ha tres formes d'escriure les hores vàlides: h:m:s, h:m, h. Ha introduït: $time")
