package org.weekly.schedule.dayTime.domain

class InvalidDayHour(hours: Int) :
    Exception("Les hores d'un dia només poden ser representats amb dígits de 0 a 23. Ha introduït $hours")
