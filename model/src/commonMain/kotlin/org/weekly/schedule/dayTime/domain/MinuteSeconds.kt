package org.weekly.schedule.dayTime.domain

data class MinuteSeconds(val seconds: UByte) {
    init {
        checkSeconds(seconds.toInt())
    }

    fun isZero(): Boolean = seconds.toInt() == 0
    override fun toString(): String = seconds.toString()
    operator fun compareTo(other: MinuteSeconds): Int = seconds.compareTo(other.seconds)

    companion object {
        fun of(seconds: Int): MinuteSeconds {
            checkSeconds(seconds)
            return MinuteSeconds(seconds.toUByte())
        }

        fun of(seconds: String): MinuteSeconds = seconds
            .toIntOrNull()
            ?.let(MinuteSeconds::of)
            ?: throw IsNotDigit(seconds, "els segons")

        private fun checkSeconds(seconds: Int) {
            if (seconds !in 0..59) throw InvalidMinuteSeconds(seconds)
        }
    }
}
