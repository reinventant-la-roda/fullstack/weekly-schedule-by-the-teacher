package org.weekly.schedule.dayTime.domain

class InvalidHourMinutes(minutes: Int) :
    Exception("Els minuts d'una hora només poden ser representats amb dígits de 0 a 59. Ha introduït $minutes")
