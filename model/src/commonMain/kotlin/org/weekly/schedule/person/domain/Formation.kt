package org.weekly.schedule.person.domain

import org.weekly.schedule.duration.domain.Duration

data class Formation(val preference: UInt, val classes: Collection<Duration>) {
    override fun toString(): String = "{$preference, $classes}"
}
