package org.weekly.schedule.person.domain

data class TeachersAndStudents(val teachers: Teachers, val students: Students)
