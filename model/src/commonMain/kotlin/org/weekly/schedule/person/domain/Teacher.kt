package org.weekly.schedule.person.domain

import org.weekly.schedule.timeInterval.domain.TimeIntervals

data class Teacher(val name: String, val available: TimeIntervals, val busy: TimeIntervals)
