package org.weekly.schedule.person.domain

import org.weekly.schedule.timeInterval.domain.TimeIntervals

data class Student(val name: String, val available: TimeIntervals, val busy: TimeIntervals, val receive: WillReceive) {
    override fun toString(): String = "$name: avai: $available, busy: $busy, reve: $receive"
}
