package org.weekly.schedule.person.domain

data class WillReceive(val formations: Collection<Formation>) {
    override fun toString(): String = formations.joinToString(prefix = "{", postfix = "}")
}
