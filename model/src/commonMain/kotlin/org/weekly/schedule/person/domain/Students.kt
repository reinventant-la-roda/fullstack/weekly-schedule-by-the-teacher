package org.weekly.schedule.person.domain

data class Students(val students: Collection<Student>) {
    init {
        areDistinct()
    }

    private fun areDistinct() {
        val names = students.map(Student::name)
        check(names.size == names.toSet().size)
    }
}
