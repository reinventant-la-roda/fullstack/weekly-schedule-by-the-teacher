package org.weekly.schedule.person.domain

data class Teachers(val teachers: Collection<Teacher>) {
    init {
        areDistinct()
    }

    private fun areDistinct() {
        val names = teachers.map(Teacher::name)
        check(names.size == names.toSet().size)
    }
}
