package org.weekly.schedule.timeInterval.domain

class IntersectionTimeIntervals(a: TimeInterval, b: TimeInterval) :
    Exception("Hi ha una intersecció superposada en {$a, $b}")
