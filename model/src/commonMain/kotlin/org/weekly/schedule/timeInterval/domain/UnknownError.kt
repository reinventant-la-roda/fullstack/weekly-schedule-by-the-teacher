package org.weekly.schedule.timeInterval.domain

class UnknownError(input: String) : Exception("Repasseu l'input. Sembla que hi hagí algun error: $input")
