package org.weekly.schedule.timeInterval.domain

class NotSortedListOnTimeIntervals(intervals: Collection<TimeInterval>) :
    Exception("La llista no està ordenada dificultant les operacions. Heu introduït: $intervals")
