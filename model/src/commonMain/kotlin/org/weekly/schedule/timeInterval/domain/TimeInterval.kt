package org.weekly.schedule.timeInterval.domain

import org.weekly.schedule.weekTime.domain.WeekTime

data class TimeInterval(val init: WeekTime, val end: WeekTime) : Comparable<TimeInterval> {
    init {
        if (init == end) throw ThisIsNotInterval(this)
    }

    fun haveIntersection(other: TimeInterval): Boolean =
        init in other || end in other || other.init in this || other.end in this
                || init == other.init

    operator fun contains(other: WeekTime): Boolean =
        end < init && (init < other || other < end)
                || init < other && other < end

    override operator fun compareTo(other: TimeInterval): Int = init.compareTo(other.init)

    override fun toString() = if (init.week == end.week) "(${init.week} ${init.time}-${end.time})" else "($init, $end)"

    companion object {
        fun of(interval: String): List<TimeInterval> {
            var week = ""
            return interval
                .split(",")
                .map { intervalString ->
                    intervalString
                        .trim()
                        .split("-")
                        .map { weekTimeString ->
                            val weekSplit = weekTimeString.trim().split("""\s++""".toRegex())
                            when (weekSplit.size) {
                                2 -> {
                                    week = weekSplit[0]
                                    WeekTime.of(week, weekSplit[1])
                                }

                                1 -> WeekTime.of(week, weekSplit[0])
                                else -> throw UnknownError(interval)
                            }
                        }
                        .let { TimeInterval(it.first(), it.last()) }
                }
        }
    }
}
