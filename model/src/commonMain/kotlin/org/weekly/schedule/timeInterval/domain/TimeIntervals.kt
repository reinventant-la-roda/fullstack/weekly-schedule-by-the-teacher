package org.weekly.schedule.timeInterval.domain

data class TimeIntervals(val intervals: Collection<TimeInterval>) {
    init {
        checkTimeIntervals()
    }

    operator fun minus(other: TimeIntervals): TimeIntervals {
        if (intervals.isEmpty() || other.intervals.isEmpty()) return this
        if (intervals.last().let { it.init > it.end }) TODO()
        if (other.intervals.last().let { it.init > it.end }) TODO()

        val minuendIterator = intervals.iterator()
        val subtrahendIterator = other.intervals.iterator()
        val rest = mutableListOf<TimeInterval>()
        var minuend: TimeInterval? = minuendIterator.next()
        var subtrahend: TimeInterval? = subtrahendIterator.next()

        while (minuend != null && subtrahend != null) {
            if (minuend.init < subtrahend.init) {
                rest += TimeInterval(minuend.init, minOf(minuend.end, subtrahend.init))
            }
            if (minuend.end > subtrahend.end) {
                minuend = TimeInterval(maxOf(minuend.init, subtrahend.end), minuend.end)
                subtrahend = if (subtrahendIterator.hasNext()) subtrahendIterator.next() else null
            } else {
                minuend = if (minuendIterator.hasNext()) minuendIterator.next() else null
            }
        }

        minuend?.let(rest::add)
        rest.addAll(minuendIterator.asSequence())
        return rest.sorted().let(::TimeIntervals)
    }

    operator fun plus(other: TimeIntervals): TimeIntervals {
        if (intervals.isEmpty() && other.intervals.isEmpty()) return other
        if (intervals.isNotEmpty() && intervals.last().let { it.init > it.end }) TODO()
        if (other.intervals.isNotEmpty() && other.intervals.last().let { it.init > it.end }) TODO()

        val iterator = (intervals + other.intervals).sorted().iterator()
        val sum = mutableListOf<TimeInterval>()
        var m: TimeInterval?
        var n: TimeInterval? = iterator.next()

        m = n
        while (m != null) {
            n = if (iterator.hasNext()) iterator.next() else null

            if (n != null && n.init <= m.end)
                m = TimeInterval(m.init, maxOf(m.end, n.end))
            else {
                sum += m
                m = n
            }
        }
        return sum.let(::TimeIntervals)
    }

    override fun toString(): String = if (intervals.isNotEmpty()) "{${intervals.joinToString()}}" else "ø"

    private fun checkTimeIntervals() {
        if (intervals.size in 0..1) return

        intervals
            .asSequence()
            .zipWithNext { a, b ->
                if (a.init >= b.init) throw NotSortedListOnTimeIntervals(intervals)
                if (a.init > a.end || a.end > b.init) throw IntersectionTimeIntervals(a, b)
            }
        val first = intervals.first()
        val last = intervals.last()
        if (first.haveIntersection(last)) throw IntersectionTimeIntervals(first, last)
    }

    companion object {
        fun of(input: Collection<String>): TimeIntervals = input
            .map(TimeInterval::of)
            .flatten()
            .sorted()
            .let(::TimeIntervals)

        fun of(vararg input: String): TimeIntervals = of(input.toList())
    }
}
