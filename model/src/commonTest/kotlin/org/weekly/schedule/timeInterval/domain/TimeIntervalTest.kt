package org.weekly.schedule.timeInterval.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.*
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.weekly.schedule.weekTime.domain.WeekTime

class TimeIntervalTest {
    @Nested
    inner class Init {
        @Test
        fun correct() {
            TimeInterval.of("dilluns 17-18").shouldContainExactlyInAnyOrder(
                TimeInterval(
                    init = WeekTime.of("dilluns", "17"),
                    end = WeekTime.of("dilluns", "18")
                )
            )
            TimeInterval.of("dimecres 16:15-17:10, 18-19").shouldContainExactlyInAnyOrder(
                TimeInterval(
                    init = WeekTime.of("dimecres", "16:15"),
                    end = WeekTime.of("dimecres", "17:10")
                ),
                TimeInterval(
                    init = WeekTime.of("dimecres", "18"),
                    end = WeekTime.of("dimecres", "19")
                ),
            )
            TimeInterval.of("dijous 20:15:12-dilluns 8:20").shouldContainExactlyInAnyOrder(
                TimeInterval(
                    init = WeekTime.of("dijous", "20:15:12"),
                    end = WeekTime.of("dilluns", "8:20")
                )
            )
            TimeInterval.of("dimarts 12:24 - 14:42").shouldContainExactlyInAnyOrder(
                TimeInterval(
                    init = WeekTime.of("dimarts", "12:24"),
                    end = WeekTime.of("dimarts", "14:42")
                )
            )
        }

        @Test
        fun sameInterval() {
            shouldThrow<ThisIsNotInterval> { TimeInterval.of("dimarts 17:24-17:24") }
        }

        @Test
        fun badFormat() {
            shouldThrow<UnknownError> { TimeInterval.of("dimarts 1 2 :24 - 14:42 , 12") }
        }
    }

    @Test
    fun intersection() {
        val interval = "dimecres 9-10".let(::getInterval)
        interval shouldHaveIntersection interval
        "dilluns 10-9".let(::getInterval) shouldHaveIntersection interval
        "dimecres 9-8".let(::getInterval) shouldHaveIntersection interval
        "dimecres 8-11".let(::getInterval) shouldHaveIntersection interval
        "divendres 10-12".let(::getInterval) shouldHaveNotIntersection interval
        "dilluns 8-dimecres 9:30".let(::getInterval) shouldHaveIntersection interval
        "dimecres 9:59:59-10:0:1".let(::getInterval) shouldHaveIntersection interval
        "dimecres 10-11".let(::getInterval) shouldHaveNotIntersection interval
    }

    @Nested
    inner class Contains {
        @Test
        fun contain() {
            val interval = "dilluns 9-10".let(::getInterval)
            WeekTime.of("dilluns", "10") shouldBeNotIn interval
            WeekTime.of("dilluns", "9:59:59") shouldBeIn interval
            WeekTime.of("dilluns", "9") shouldBeNotIn interval
            WeekTime.of("dilluns", "9:0:1") shouldBeIn interval
            WeekTime.of("dijous", "9:30") shouldBeNotIn interval
        }

        @Test
        fun inverse() {
            val interval = "dijous 11-10".let(::getInterval)
            WeekTime.of("dijous", "10") shouldBeNotIn interval
            WeekTime.of("dijous", "9:59:59") shouldBeIn interval
            WeekTime.of("dijous", "11") shouldBeNotIn interval
            WeekTime.of("dijous", "11:0:1") shouldBeIn interval
            WeekTime.of("dijous", "10:30") shouldBeNotIn interval
            WeekTime.of("dilluns", "3") shouldBeIn interval
        }
    }

    @Test
    fun string() {
        TimeInterval.of("dilluns 8:0:0-10:1")
            .first()
            .toString() shouldBe "(dilluns 8-10:1)"

        TimeInterval.of("dilluns 15:0-dimarts 18:0:1")
            .first()
            .toString() shouldBe "(dilluns 15, dimarts 18:0:1)"
    }
}

private fun getInterval(init: String): TimeInterval = init.let(TimeInterval::of).first()

private infix fun WeekTime.shouldBeIn(interval: TimeInterval) = this should shouldContain(interval)
private infix fun WeekTime.shouldBeNotIn(interval: TimeInterval) = this shouldNot shouldContain(interval)
private infix fun TimeInterval.shouldHaveIntersection(a: TimeInterval) = this should shouldIntersect(a)
private infix fun TimeInterval.shouldHaveNotIntersection(a: TimeInterval) = this shouldNot shouldIntersect(a)

private fun shouldContain(interval: TimeInterval) = Matcher<WeekTime> { time ->
    MatcherResult(
        time in interval,
        { "El moment $time no és detectat dins del interval $interval" },
        { "El moment $time és detectat dins del interval $interval" },
    )
}

private fun shouldIntersect(a: TimeInterval) = Matcher<TimeInterval> { b ->
    MatcherResult(
        a.haveIntersection(b),
        { "Els intervals $a i $b no són detectats amb intersecció" },
        { "Els intervals $a i $b són detectats amb intersecció" }
    )
}
