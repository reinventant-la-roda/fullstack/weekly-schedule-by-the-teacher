package org.weekly.schedule.timeInterval.domain

import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class TimeIntervalsTest {
    @Nested
    inner class Minus {
        @Nested
        inner class SubtrahendBefore {
            @Test
            fun withoutEffect() {
                val minuend = "dimecres 16-18, 19-20".toIntervals()
                val subtrahend = "dilluns 10-dimarts 10, dimecres 10-16".toIntervals()
                minuend - subtrahend shouldBe minuend
            }

            @Test
            fun isEmpty() {
                val minuend = "dijous 10-15".toIntervals()
                (minuend - minuend).intervals.shouldBeEmpty()
                minuend - "dijous 10-16".toIntervals() shouldBe TimeIntervals.of(emptyList())
                minuend - "dijous 8-15".toIntervals() shouldBe TimeIntervals.of(emptyList())
                minuend - "dijous 8-divendres 15".toIntervals() shouldBe TimeIntervals.of(emptyList())
            }

            @Test
            fun simpleMinus() {
                val minuend = "dimarts 20-22".toIntervals()
                val subtrahend = "dimarts 19-21".toIntervals()
                val difference = "dimarts 21-22".toIntervals()

                minuend - subtrahend shouldBe difference
            }

            @Test
            fun multipleMinuends() {
                val minuend = "dimarts 10-13, divendres 10-13, diumenge 10-20".toIntervals()
                val subtrahend = "dilluns 10-diumenge 11".toIntervals()
                val difference = "diumenge 11-20".toIntervals()

                minuend - subtrahend shouldBe difference
            }

            @Test
            fun concatenateSubtrahend() {
                val minuend = "dissabte 5-20".toIntervals()
                val subtrahend = "dilluns 10-dissabte 10, 10-11, 11-12, 12-15".toIntervals()
                val difference = "dissabte 15-20".toIntervals()

                minuend - subtrahend shouldBe difference
            }
        }

        @Nested
        inner class SubtrahendAfter {
            @Test
            fun withoutEffect() {
                val minuend = "dimecres 16-18, 19-20".toIntervals()
                val subtrahend = "dimecres 18-19, 20-21, 22-23".toIntervals()

                minuend - subtrahend shouldBe minuend
            }

            @Test
            fun simpleMinus() {
                val minuend = "dimarts 19-21".toIntervals()
                val subtrahend = "dimarts 20-22".toIntervals()
                val difference = "dimarts 19-20".toIntervals()

                minuend - subtrahend shouldBe difference
            }

            @Test
            fun multipleMinuends() {
                val minuend = "divendres 10-20, 22-23".toIntervals()
                val subtrahend = "divendres 9-11, 12-13, 15-17, 19-21".toIntervals()
                val difference = "divendres 11-12, 13-15, 17-19, 22-23".toIntervals()

                minuend - subtrahend shouldBe difference
            }
        }
    }

    @Nested
    inner class Plus {
        @Test
        fun concatenate() {
            val a = "dilluns 1-2, 2-3, 3-4, 4-5, 5-6, 6-7, 7-8, 8-9".toIntervals()
            val b = TimeIntervals(emptyList())

            a + b shouldBe "dilluns 1-9".toIntervals()
        }

        @Test
        fun complex() {
            val a = "dimarts 10-12, 13-14, dijous 5-8".toIntervals()
            val b = "dimarts 11-13, dijous 10-20".toIntervals()
            val sum = "dimarts 10-14, dijous 5-8, 10-20".toIntervals()

            a + b shouldBe sum
        }
    }
}

fun String.toIntervals() = this.let(::listOf).let(TimeIntervals::of)
