package org.weekly.schedule.weekTime.domain

import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.comparables.shouldBeGreaterThan
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class WeekTimeTest {
    @Nested
    inner class Compare {
        @Test
        fun week() {
            WeekTime.of("dilluns", "0") shouldBeEqualComparingTo WeekTime.of("dilluns", "0")
            WeekTime.of("dilluns", "23:59:59") shouldBeLessThan WeekTime.of("dimecres", "0")
            WeekTime.of("dijous", "0:0:7") shouldBeGreaterThan WeekTime.of("dimecres", "23:59:59")
        }

        @Test
        fun time() {
            WeekTime.of("dilluns", "14:31:7") shouldBeEqualComparingTo WeekTime.of("dilluns", "14:31:7")
            WeekTime.of("dimarts", "23:59:58") shouldBeLessThan WeekTime.of("dimarts", "23:59:59")
            WeekTime.of("diumenge", "0:0:7") shouldBeGreaterThan WeekTime.of("diumenge", "0:0:6")
        }
    }
    @Test
    fun checkToString() {
        WeekTime.of("dijous", "16:20:42").toString() shouldBe "dijous 16:20:42"
        WeekTime.of("divendres", "17:20").toString() shouldBe "divendres 17:20"
    }
}
