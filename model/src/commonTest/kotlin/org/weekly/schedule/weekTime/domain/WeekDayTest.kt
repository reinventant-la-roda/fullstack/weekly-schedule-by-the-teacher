package org.weekly.schedule.weekTime.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class WeekDayTest {
    @Nested
    inner class Init {
        @Test
        fun works() {
            WeekDay.of("dilluns") shouldBe WeekDay.MONDAY
            WeekDay.of("dimarts") shouldBe WeekDay.TUESDAY
            WeekDay.of("dimecres") shouldBe WeekDay.WEDNESDAY
            WeekDay.of("dijous") shouldBe WeekDay.THURSDAY
            WeekDay.of("divendres") shouldBe WeekDay.FRIDAY
            WeekDay.of("dissabte") shouldBe WeekDay.SATURDAY
            WeekDay.of("diumenge") shouldBe WeekDay.SUNDAY

            WeekDay.of(0) shouldBe WeekDay.MONDAY
            WeekDay.of(1) shouldBe WeekDay.TUESDAY
            WeekDay.of(2) shouldBe WeekDay.WEDNESDAY
            WeekDay.of(3) shouldBe WeekDay.THURSDAY
            WeekDay.of(4) shouldBe WeekDay.FRIDAY
            WeekDay.of(5) shouldBe WeekDay.SATURDAY
            WeekDay.of(6) shouldBe WeekDay.SUNDAY
        }

        @Test
        fun incorrect() {
            shouldThrow<InvalidWeekName> { WeekDay.of("vendredi") }
            shouldThrow<InvalidWeekNumber> { WeekDay.of(69) }
        }

        @Test
        fun caseInsensitive() {
            WeekDay.of("dilluns") shouldBe WeekDay.of("DILLUNS")
            WeekDay.of("dimarts") shouldBe WeekDay.of("DImarts")
            WeekDay.of("dimecres") shouldBe WeekDay.of("Dimecres")
            WeekDay.of("dijous") shouldBe WeekDay.of("diJous")
            WeekDay.of("divendres") shouldBe WeekDay.of("diVENdres")
            WeekDay.of("dissabte") shouldBe WeekDay.of("diSSabte")
            WeekDay.of("diumenge") shouldBe WeekDay.of("diumengE")
        }
    }

    @Test
    fun sorted() {
        WeekDay.entries.shuffled().sorted().shouldContainExactlyInAnyOrder(
            WeekDay.MONDAY,
            WeekDay.TUESDAY,
            WeekDay.WEDNESDAY,
            WeekDay.THURSDAY,
            WeekDay.FRIDAY,
            WeekDay.SATURDAY,
            WeekDay.SUNDAY,
        )
    }

    @Test
    fun checkToString() {
        WeekDay.MONDAY.toString() shouldBe "dilluns"
        WeekDay.TUESDAY.toString() shouldBe "dimarts"
        WeekDay.WEDNESDAY.toString() shouldBe "dimecres"
        WeekDay.THURSDAY.toString() shouldBe "dijous"
        WeekDay.FRIDAY.toString() shouldBe "divendres"
        WeekDay.SATURDAY.toString() shouldBe "dissabte"
        WeekDay.SUNDAY.toString() shouldBe "diumenge"
    }
}
