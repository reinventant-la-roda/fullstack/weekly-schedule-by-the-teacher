package org.weekly.schedule.dayTime.domain

import io.kotest.assertions.throwables.shouldThrow
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class DayHoursTest {
    @Nested
    inner class Init {
        @Test
        fun correct() {
            DayHours.of(0)
            DayHours.of("5")
            DayHours.of(10)
            DayHours.of("15")
            DayHours.of(23)
        }

        @Test
        fun invalidHour() {
            shouldThrow<InvalidDayHour> { DayHours.of("24") }
            shouldThrow<InvalidDayHour> { DayHours.of(255) }
            shouldThrow<InvalidDayHour> { DayHours.of("256") }
            shouldThrow<InvalidDayHour> { DayHours.of(-3) }
            shouldThrow<InvalidDayHour> { DayHours.of("-1") }
        }

        @Test
        fun nonDigit() {
            shouldThrow<IsNotDigit> { DayHours.of("nonDigit") }
        }
    }
}
