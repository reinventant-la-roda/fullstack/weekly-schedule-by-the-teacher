package org.weekly.schedule.dayTime.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.comparables.shouldBeGreaterThan
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class DayTimeTest {
    @Test
    fun checkToString() {
        DayTime.of("13").toString() shouldBe "13"
        DayTime.of("7:43").toString() shouldBe "7:43"
        DayTime.of("16:20:42").toString() shouldBe "16:20:42"
    }

    @Nested
    inner class Compare {
        @Test
        fun hours() {
            DayTime.of("17") shouldBeEqualComparingTo DayTime.of("17")
            DayTime.of("7") shouldBeLessThan DayTime.of("9")
            DayTime.of("23") shouldBeGreaterThan DayTime.of("5")
        }

        @Test
        fun minutes() {
            DayTime.of("14:14") shouldBeEqualComparingTo DayTime.of("14:14")
            DayTime.of("15:14") shouldBeLessThan DayTime.of("15:15")
            DayTime.of("16:4") shouldBeGreaterThan DayTime.of("16:1")
        }

        @Test
        fun seconds() {
            DayTime.of("4:14:41") shouldBeEqualComparingTo DayTime.of("4:14:41")
            DayTime.of("5:14:30") shouldBeLessThan DayTime.of("5:14:31")
            DayTime.of("6:4:2") shouldBeGreaterThan DayTime.of("6:4:1")
        }
    }

    @Nested
    inner class Init {
        @Test
        fun normal() {
            DayTime.of("13") shouldBe DayTime(DayHours.of(13), HourMinutes.of(0), MinuteSeconds.of(0))
            DayTime.of("7:43") shouldBe DayTime(DayHours.of(7), HourMinutes.of(43), MinuteSeconds.of(0))
            DayTime.of("16:20:42") shouldBe DayTime(DayHours.of(16), HourMinutes.of(20), MinuteSeconds.of(42))
        }

        @Test
        fun invalid() {
            shouldThrow<IsNotDigit> { DayTime.of("") }
            shouldThrow<InvalidDayTime> { DayTime.of("hores:minuts:segons:altres") }
        }
    }
}
