package org.weekly.schedule.dayTime.domain

import io.kotest.assertions.throwables.shouldThrow
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class MinuteSecondsTest {
    @Nested
    inner class Innit {
        @Test
        fun correct() {
            MinuteSeconds.of(0)
            MinuteSeconds.of("15")
            MinuteSeconds.of(30)
            MinuteSeconds.of("45")
            MinuteSeconds.of(59)
        }

        @Test
        fun invalidHour() {
            shouldThrow<InvalidMinuteSeconds> { MinuteSeconds.of("60") }
            shouldThrow<InvalidMinuteSeconds> { MinuteSeconds.of(255) }
            shouldThrow<InvalidMinuteSeconds> { MinuteSeconds.of("256") }
            shouldThrow<InvalidMinuteSeconds> { MinuteSeconds.of(-3) }
            shouldThrow<InvalidMinuteSeconds> { MinuteSeconds.of("-1") }
        }

        @Test
        fun nonDigit() {
            shouldThrow<IsNotDigit> { MinuteSeconds.of("doncs no") }
        }
    }
}
