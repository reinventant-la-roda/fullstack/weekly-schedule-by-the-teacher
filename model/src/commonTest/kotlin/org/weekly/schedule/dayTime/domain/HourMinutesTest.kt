package org.weekly.schedule.dayTime.domain

import io.kotest.assertions.throwables.shouldThrow
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class HourMinutesTest {
    @Nested
    inner class Init {
        @Test
        fun correct() {
            HourMinutes.of(0)
            HourMinutes.of("15")
            HourMinutes.of(30)
            HourMinutes.of("45")
            HourMinutes.of(59)
        }

        @Test
        fun invalidHour() {
            shouldThrow<InvalidHourMinutes> { HourMinutes.of("60") }
            shouldThrow<InvalidHourMinutes> { HourMinutes.of(255) }
            shouldThrow<InvalidHourMinutes> { HourMinutes.of("256") }
            shouldThrow<InvalidHourMinutes> { HourMinutes.of(-3) }
            shouldThrow<InvalidHourMinutes> { HourMinutes.of("-1") }
        }

        @Test
        fun nonDigit() {
            shouldThrow<IsNotDigit> { HourMinutes.of("doncs no") }
        }
    }
}
