package org.weekly.schedule.duration.domain

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import kotlin.test.Test

class DurationTest {
    @Nested
    inner class Init {
        @Test
        fun correct() {
            Duration.of("3d 4h 30m 16s") shouldBe Duration(
                (3 * 24 * 60 * 60 + 4 * 60 * 60 + 30 * 60 + 16).toUInt()
            )
            Duration.of("4d") shouldBe Duration((4 * 24 * 60 * 60).toUInt())
            Duration.of("15h") shouldBe Duration((15 * 60 * 60).toUInt())
            Duration.of("42m") shouldBe Duration(42u * 60u)
            Duration.of("3s") shouldBe Duration(3u)
            Duration.of("300m") shouldBe Duration(300u * 60u)
        }

        @Test
        fun incorrect() {
            shouldThrow<EmptyDurationConfiguration> { Duration.of("") }
            shouldThrow<NeedPositiveDuration> { Duration.of("0d 0h 0m 0s") }
            shouldThrow<ExceededTimeWeek> { Duration.of("7d") }
            shouldThrow<ExceededTimeWeek> { Duration.of("6d 23h 59m 60s") }
        }
    }

    @Test
    fun string() {
        Duration.of("1s").toString() shouldBe "1s"
        Duration.of("1m").toString() shouldBe "1m"
        Duration.of("1h").toString() shouldBe "1h"
        Duration.of("1d").toString() shouldBe "1d"
        Duration.of("6d 23h 59m 59s").toString() shouldBe "6d 23h 59m 59s"
    }
}
