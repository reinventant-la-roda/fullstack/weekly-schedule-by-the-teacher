# https://www.gnu.org/software/make/manual/make.html
help:
	@echo "help             show this help"
	@echo "build            compile and execute tests"
	@echo "install          generate bin for execute this project"
	@echo "update           check the updates available"
	@echo "clean            clean the builds"

build: gradlew
	./gradlew \
		--warning-mode all \
		build \
		koverHtmlReport

install: gradlew
	./gradlew InstallDist
	mkdir -p build
	_SCHEDULE_WEEK_COMPLETE=bash command/build/install/command/bin/command > build/schedule-week-completion.sh
	echo "source $(shell realpath build/schedule-week-completion.sh)" > build/alias.sh
	echo "alias schedule-week='$(shell realpath command/build/install/command/bin/command)'" >> build/alias.sh
	@echo source $(shell realpath build/alias.sh)
	@echo schedule-week

update: gradlew
	./gradlew dependencyUpdates

clean: gradlew
	./gradlew clean

gradlew:
	# XXX is not a prerequisite because we don't want to rebuild when is updated
	touch settings.gradle.kts
	# We want the latest version
	podman pull gradle
	podman run \
		--rm \
		--volume "${PWD}":/tmp/gradle/project \
		--workdir /tmp/gradle/project \
		gradle \
		gradle wrapper

.PHONY: help \
	build \
	install \
	update \
	clean \
