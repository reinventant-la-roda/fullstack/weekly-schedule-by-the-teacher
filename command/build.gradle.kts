plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    application
    id("org.jetbrains.kotlinx.kover")
}

dependencies {
    implementation(project(":model"))
    implementation(libs.clikt)
    implementation(libs.serialization)
    implementation(libs.mordant.terminal)
    implementation(libs.logging)

    testImplementation(kotlin("test"))
    testImplementation(libs.kotest)
    testImplementation(libs.kotest.json)
}

application {
    mainClass.set("org.weekly.schedule.command.MainKt")
}
