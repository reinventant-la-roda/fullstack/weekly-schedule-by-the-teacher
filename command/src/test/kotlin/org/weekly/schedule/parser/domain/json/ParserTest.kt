package org.weekly.schedule.parser.domain.json

import io.klogging.Klogger
import io.klogging.logger
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.runBlocking
import org.weekly.schedule.duration.domain.Duration
import org.weekly.schedule.person.domain.Formation
import org.weekly.schedule.person.domain.Student
import org.weekly.schedule.person.domain.WillReceive
import org.weekly.schedule.timeInterval.domain.TimeIntervals
import kotlin.io.path.Path
import kotlin.io.path.readText
import kotlin.test.Test

class ParserTest {
    @Test
    fun initialize() {
        """
            {
              "template": [
                {
                  "id": "petits",
                  "scheduleAvailable": [ "dilluns 16-17, 18-19", "dimecres 16-17, 18-19" ],
                  "receive": [ { "preference": 1, "classes": [ "30m", "1h" ] } ]
                }
              ],
              "teachers": [],
              "students": [
                { "name": "Marc", "templateId": [ "petits" ] }
              ]
            }
        """
            .trimIndent()
            .let(Parser::of) shouldBe Parser(
            template = listOf(
                StudentTemplateParse(
                    "petits",
                    scheduleAvailable = listOf("dilluns 16-17, 18-19", "dimecres 16-17, 18-19"),
                    receive = listOf(ReceiveParse(1u, listOf("30m", "1h")))
                )
            ),
            teachers = emptyList(),
            students = listOf(StudentParse("Marc", templateId = listOf("petits")))
        )
    }

    @Test
    fun example(): Unit = runBlocking {
        val log: Klogger = logger<ParserTest>()
        this::class.java.classLoader
            .getResource("example.json")!!
            .path.let(::Path)
            .readText()
            .let(Parser::of)
            .getConfig(log).students.students
            .shouldContainExactlyInAnyOrder(
                getStudent("Joan", "dilluns 17:30-18:15, dimecres 17:30-19", "dilluns 17-19:30, dimecres 17-19:30"),
                getStudent("Maria", "dimarts 17:45-19:15, dijous 17:45-18:30"),
                getStudent("Arlet", "dilluns 17:30-18:15, dimecres 17:30-19"),
                getStudent("Rita", "dilluns 18:15-19, dimecres 18:15-19"),
                getStudent("Adrià", "dilluns 18:15-19, dimecres 18:15-19, dimarts 18:30-19:15"),
                getStudent("Arnau", "dimarts 17:45-19:15, dijous 18:30-19:15"),
                Student(
                    name = "Martina",
                    available = Utils.emptyTimeIntervals(),
                    busy = Utils.emptyTimeIntervals(),
                    receive = WillReceive(
                        emptyList()
                    )
                ),
                getStudent("Ariadna", "divendres 16-18"),
                getStudent("Jan", "dimarts 18-20")
            )
    }
}

fun getStudent(name: String, busy: String, available: String = ""): Student = Student(
    name = name,
    available = if (available == "") Utils.emptyTimeIntervals() else TimeIntervals.of(available),
    busy = TimeIntervals.of(busy),
    receive = WillReceive(
        listOf(Formation(preference = 42u, listOf(Duration.of("30m"))))
    )
)
