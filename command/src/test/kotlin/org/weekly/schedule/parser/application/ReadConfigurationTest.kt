package org.weekly.schedule.parser.application

import io.klogging.Klogger
import io.klogging.logger
import io.kotest.matchers.collections.shouldHaveSize
import kotlinx.coroutines.runBlocking
import kotlin.io.path.Path
import kotlin.test.Test

class ReadConfigurationTest {
    @Test
    fun readConfiguration(): Unit = runBlocking {
        val jsonPath = this::class.java.classLoader.getResource("example.json")!!.path.let(::Path)
        val log: Klogger = logger<ReadConfigurationTest>()

        val config = ReadConfiguration.exec(jsonPath, log)

        config.students.students shouldHaveSize 9
    }
}
