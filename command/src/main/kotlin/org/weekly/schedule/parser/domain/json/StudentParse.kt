package org.weekly.schedule.parser.domain.json

import kotlinx.serialization.Serializable

@Serializable
data class StudentParse(
    val name: String,
    val templateId: Collection<String>? = null,
    val scheduleBusy: Collection<String>? = null,
    val scheduleAvailable: Collection<String>? = null,
    val receive: Collection<ReceiveParse>? = null
) {
    fun parsing(): StudentCommand = StudentCommand(
        name = name,
        busy = scheduleBusy.let(Utils::getTimeIntervals),
        available = scheduleAvailable.let(Utils::getTimeIntervals),
        receive = receive.let(Utils::getReceive)
    )
}
