package org.weekly.schedule.parser.application

import io.klogging.Klogger
import io.klogging.context.withLogContext
import org.weekly.schedule.parser.domain.json.Parser
import org.weekly.schedule.parser.domain.json.Utils
import org.weekly.schedule.person.domain.TeachersAndStudents
import java.nio.file.Path
import kotlin.io.path.readText
import kotlin.system.measureTimeMillis

class ReadConfiguration {
    companion object {
        suspend fun exec(config: Path, log: Klogger): TeachersAndStudents {
            var result: TeachersAndStudents = Utils.emptyConfig()
            withLogContext("configFile" to config) {
                log.debug("Read config from {configFile}")

                val timeInMillis = measureTimeMillis {
                    result = config.readText().let(Parser::of).getConfig(log)
                }

                log.info(
                    "We read a total of {teachersNumber} teachers and {studentsNumber} students in {Time} ms",
                    result.teachers.teachers.size,
                    result.students.students.size,
                    timeInMillis
                )
            }
            return result
        }
    }
}
