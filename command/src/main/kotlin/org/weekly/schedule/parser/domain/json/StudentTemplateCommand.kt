package org.weekly.schedule.parser.domain.json

import org.weekly.schedule.person.domain.WillReceive
import org.weekly.schedule.timeInterval.domain.TimeIntervals

data class StudentTemplateCommand(val busy: TimeIntervals, val available: TimeIntervals, val receive: WillReceive)
