package org.weekly.schedule.parser.domain.json

import kotlinx.serialization.Serializable

@Serializable
data class TeacherParse(
    val name: String,
    val templateId: Collection<String>? = null,
    val scheduleBusy: Collection<String>? = null,
    val scheduleAvailable: Collection<String>? = null,
) {
    fun parsing(): TeacherCommand = TeacherCommand(
        name = name,
        busy = scheduleBusy.let(Utils::getTimeIntervals),
        available = scheduleAvailable.let(Utils::getTimeIntervals)
    )
}
