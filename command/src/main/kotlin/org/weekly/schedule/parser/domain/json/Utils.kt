package org.weekly.schedule.parser.domain.json

import org.weekly.schedule.duration.domain.Duration
import org.weekly.schedule.person.domain.*
import org.weekly.schedule.timeInterval.domain.TimeIntervals

class Utils {
    companion object {
        fun getTimeIntervals(str: Collection<String>?) =
            str?.let(TimeIntervals::of) ?: emptyTimeIntervals()

        fun getReceive(receiveParses: Collection<ReceiveParse>?): WillReceive = receiveParses
            ?.map { formation -> Formation(formation.preference, formation.classes.map(Duration::of)) }
            ?.let(::WillReceive)
            ?: WillReceive(emptyList())

        fun emptyTimeIntervals(): TimeIntervals = TimeIntervals(emptyList())

        fun emptyConfig(): TeachersAndStudents = TeachersAndStudents(
            teachers = Teachers(emptyList()),
            students = Students(emptyList())
        )
    }
}
