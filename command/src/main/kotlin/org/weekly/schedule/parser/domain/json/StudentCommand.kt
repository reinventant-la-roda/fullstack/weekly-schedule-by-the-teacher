package org.weekly.schedule.parser.domain.json

import org.weekly.schedule.person.domain.Student
import org.weekly.schedule.person.domain.WillReceive
import org.weekly.schedule.timeInterval.domain.TimeIntervals

class StudentCommand(
    val name: String,
    val available: TimeIntervals,
    val busy: TimeIntervals,
    val receive: WillReceive
) {
    fun toStudent(): Student = Student(name, available, busy, receive)

    operator fun plus(other: StudentTemplateCommand): StudentCommand = StudentCommand(
        name = name,
        available = available + other.available,
        busy = busy + other.busy,
        receive = (receive.formations + other.receive.formations).let(::WillReceive)
    )
}
