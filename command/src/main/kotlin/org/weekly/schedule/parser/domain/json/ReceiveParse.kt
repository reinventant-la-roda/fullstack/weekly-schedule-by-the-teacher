package org.weekly.schedule.parser.domain.json

import kotlinx.serialization.Serializable

@Serializable
data class ReceiveParse(val preference: UInt, val classes: Collection<String>)
