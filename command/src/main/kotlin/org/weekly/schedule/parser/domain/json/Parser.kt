package org.weekly.schedule.parser.domain.json

import io.klogging.Klogger
import io.klogging.context.withLogContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.weekly.schedule.person.domain.Students
import org.weekly.schedule.person.domain.Teachers
import org.weekly.schedule.person.domain.TeachersAndStudents

private const val TYPE_PARSE_LOG = "typeParse"
private const val NAME_PARSE_LOG = "nameParse"

@Serializable
data class Parser(
    val template: List<StudentTemplateParse>?,
    val teachers: List<TeacherParse>,
    val students: List<StudentParse>,
) {

    suspend fun getConfig(log: Klogger): TeachersAndStudents {
        val templates = getMapOfTemplates()
        val students = getStudents(log, templates)
        val teachers = getTeachers(log, templates)
        return TeachersAndStudents(teachers, students)
    }

    private fun getMapOfTemplates(): Map<String, StudentTemplateCommand> =
        (template
            ?.associate { it.id to it.toStudentTemplate() }
            ?: emptyMap())

    private suspend fun getStudents(log: Klogger, templates: Map<String, StudentTemplateCommand>): Students =
        students.map { studentParse ->
            withLogContext(TYPE_PARSE_LOG to "student", NAME_PARSE_LOG to studentParse.name) {
                (studentParse.templateId ?: emptyList())
                    .mapNotNull { templates.getWarning(log, it) }
                    .fold(studentParse.parsing()) { acc, studentTemplateCommand -> acc + studentTemplateCommand }
                    .toStudent()
            }
        }.let(::Students)

    private suspend fun getTeachers(log: Klogger, templates: Map<String, StudentTemplateCommand>): Teachers =
        teachers.map { teacherParse ->
            withLogContext(TYPE_PARSE_LOG to "teacher", NAME_PARSE_LOG to teacherParse.name) {
                (teacherParse.templateId ?: emptyList())
                    .mapNotNull { templates.getWarning(log, it) }
                    .fold(teacherParse.parsing()) { acc, studentTemplateCommand -> acc + studentTemplateCommand }
                    .toTeacher()
            }
        }.let(::Teachers)

    companion object {
        fun of(init: String): Parser = Json.decodeFromString(init)
    }
}

suspend fun Map<String, StudentTemplateCommand>.getWarning(log: Klogger, key: String): StudentTemplateCommand? {
    val result = this[key]
    if (result != null) return result

    withLogContext("templateId" to key) {
        log.warn("The {$TYPE_PARSE_LOG} with name {$NAME_PARSE_LOG} has not a existent template {templateId}")
    }
    return null
}
