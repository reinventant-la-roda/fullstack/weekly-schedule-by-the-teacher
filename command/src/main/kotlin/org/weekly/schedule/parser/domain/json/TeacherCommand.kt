package org.weekly.schedule.parser.domain.json

import org.weekly.schedule.person.domain.Teacher
import org.weekly.schedule.timeInterval.domain.TimeIntervals

data class TeacherCommand(val name: String, val busy: TimeIntervals, val available: TimeIntervals) {
    fun toTeacher(): Teacher = Teacher(name, available = available, busy = busy)
    operator fun plus(other: StudentTemplateCommand) = TeacherCommand(
        name = name,
        busy = busy + other.busy,
        available = available + other.available
    )
}
