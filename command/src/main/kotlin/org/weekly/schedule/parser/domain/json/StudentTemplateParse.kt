package org.weekly.schedule.parser.domain.json

import kotlinx.serialization.Serializable

@Serializable
data class StudentTemplateParse(
    val id: String,
    val scheduleBusy: Collection<String>? = null,
    val scheduleAvailable: Collection<String>? = null,
    val receive: Collection<ReceiveParse>? = null
) {
    fun toStudentTemplate(): StudentTemplateCommand =
        StudentTemplateCommand(
            available = scheduleAvailable.let(Utils::getTimeIntervals),
            busy = scheduleBusy.let(Utils::getTimeIntervals),
            receive = receive.let(Utils::getReceive)
        )
}
