package org.weekly.schedule.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.mordant.table.table
import com.github.ajalt.mordant.terminal.Terminal
import org.weekly.schedule.person.domain.Student
import org.weekly.schedule.person.domain.Teacher
import org.weekly.schedule.person.domain.TeachersAndStudents

private val t: Terminal by lazy { Terminal() }

class Students : CliktCommand(help = "Mostra els alumnes") {
    private val config by requireObject<TeachersAndStudents>()
    override fun run() {
        config.students.students.also(::showStudents)
    }
}

class Teachers : CliktCommand(help = "Mostra els professors") {
    private val config by requireObject<TeachersAndStudents>()
    override fun run() {
        config.teachers.teachers.also(::showTeachers)
    }
}

class All : CliktCommand(help = "Mostra alumnes i professorat") {
    private val config by requireObject<TeachersAndStudents>()
    override fun run() {
        echo("Professorat:")
        config.teachers.teachers.also(::showTeachers)
        echo("Alumnes:")
        config.students.students.also(::showStudents)
    }
}

class Show : CliktCommand(help = "Mostra les configuracions per apreciar si la configuració és correcta") {
    init {
        subcommands(Students(), Teachers(), All())
    }

    override fun run() {}
}

private fun showTeachers(teachers: Collection<Teacher>) {
    table {
        header { row("nom", "disponible", "ocupat") }
        body {
            teachers.forEach { teacher ->
                row(
                    teacher.name,
                    teacher.available.intervals.joinToString("\n"),
                    teacher.busy.intervals.joinToString("\n")
                )
            }
        }
    }.let(t::println)
}

private fun showStudents(students: Collection<Student>) {
    val table = table {
        header { row("nom", "disponible", "ocupat", "rebrà") }
        body {
            students.forEach { student ->
                row(
                    student.name,
                    student.available.intervals.joinToString("\n"),
                    student.busy.intervals.joinToString("\n"),
                    student.receive.formations.map { "${it.preference} ${it.classes}" }.joinToString("\n")
                )
            }
        }
    }
    t.println(table)
}
