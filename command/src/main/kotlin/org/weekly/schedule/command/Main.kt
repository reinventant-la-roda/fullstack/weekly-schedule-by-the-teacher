package org.weekly.schedule.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.switch
import com.github.ajalt.clikt.parameters.types.path
import io.klogging.Level
import io.klogging.config.STDOUT_ANSI
import io.klogging.config.loggingConfiguration
import io.klogging.context.withLogContext
import io.klogging.logger
import kotlinx.coroutines.runBlocking
import org.weekly.schedule.parser.application.ReadConfiguration
import java.nio.file.Path

class ScheduleWeek : CliktCommand(printHelpOnEmptyArgs = true) {
    private val config: Path by argument(help = "Fitxer de la configuració")
        .path(mustExist = true, canBeFile = true, canBeDir = false, mustBeReadable = true)
    private val loggingMinLogLevel: Level by option(help = "Nivell mínim per mostrar, per defecte ${Level.INFO}")
        .switch(Level.entries.associateBy { "--$it" })
        .default(Level.INFO)
    private val minDirectLog: Level by option("--directLog", help = "A partir de quin log el genera directament")
        .switch(Level.entries.associateBy { "--${it}-DIRECT" })
        .default(Level.WARN)

    init {
        subcommands(Show())
    }

    override fun run() {
        loggingConfiguration {
            minDirectLogLevel(minDirectLog)
            sink("console", STDOUT_ANSI)
            logging { fromMinLevel(loggingMinLogLevel) { toSink("console") } }
        }
        val logger = logger<ScheduleWeek>()
        runBlocking {
            withLogContext("runId" to 1) {
                currentContext.obj = ReadConfiguration.exec(config, logger)
            }
        }
    }
}

fun main(args: Array<String>) = ScheduleWeek().main(args)
